// When you change APPName, be sure to update it in mylibs/util.js
// @see http://paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
var APPNAME = {

  initSlider: function() {
    $('.flexslider').flexslider({
      animation: "slide"
    });
  },
  
  // Initializers
  common: {
    init: function() { 

    },
    finalize: function() {

    }
  },
  
  has_slider: {
    init: function() { 
      APPNAME.initSlider();
    },
    finalize: function() { 

    }
  }
};

UTIL = {
  fire: function( func,funcname, args ) {
    var namespace = APPNAME;  // indicate your obj literal namespace here

    funcname = ( funcname === undefined ) ? 'init' : funcname;
    if ( func !== '' && namespace[ func ] && typeof namespace[ func ][ funcname ] == 'function' ) {
      namespace[ func ][ funcname ]( args );
    }
  },
  loadEvents: function() {
    var bodyId = document.body.id;

    // hit up common first.
    UTIL.fire( 'common' );

    // do all the classes too.
    $.each( document.body.className.split( /\s+/ ), function( i, classnm ) {
      UTIL.fire( classnm );
      UTIL.fire( classnm, bodyId );
    });
    UTIL.fire( 'common', 'finalize' );
  }
};

$(document).ready(UTIL.loadEvents);


/* Youtube API */

function youTubeApi(target){
  var tag = document.createElement('script');
  var target = $(target);
  tag.src = "http://www.youtube.com/player_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  var player;
  function onYouTubePlayerAPIReady() {
    var $m = $(target);
    player = new YT.Player('movie_player', {
      playerVars: { 'autoplay': 1, 'controls': 0,'autohide':1,'wmode':'opaque', 'loop': 1, 'rel':0, 'showinfo':0, 'fs':0,'playlist':$m.data('video') },
      videoId: $m.data("video"),
      events: {
        'onReady': onPlayerReady}
      });
  }
}

// Sticky Elements

function sticky(element,height){
  var  mn = $(element);
    mns = "has-scrolled";
    hdr = $(height).height(),
    offset = $(element).outerHeight();

  $(window).scroll(function() {
    if( $(this).scrollTop() > hdr ) {
      if (!mn.hasClass(mns)){
        mn.addClass(mns);
        mn.before('<div class="nav-offset" style="height:' + offset + 'px;"></div>' );
      }
      
    } else {
      mn.removeClass(mns);
      $('.nav-offset').remove();
    }
  });
}
  
  

// Hide Header on on scroll down

function hasScrolled() {

  var didScroll;
  var lastScrollTop = 0;
  var delta = 5;
  var navbarHeight = $('header').outerHeight();
  console.log(navbarHeight);

  $(window).scroll(function(event){
    didScroll = true;
  });

  setInterval(function() {
    if (didScroll) {
      hasScrolled();
      didScroll = false;
    }
  }, 250);

  var st = $(this).scrollTop();
  // Make sure they scroll more than delta
  if(Math.abs(lastScrollTop - st) <= delta)
    return;
  
  // If they scrolled down and are past the navbar, add class .nav-up.
  // This is necessary so you never see what is "behind" the navbar.
  if (st > lastScrollTop && st > navbarHeight){
      // Scroll Down
      $('header').removeClass('nav-down').addClass('nav-up').css('top',-navbarHeight);
    } else {
      // Scroll Up
      if(st + $(window).height() < $(document).height()) {
        $('header').removeClass('nav-up').addClass('nav-down').css('top','0');
      }
    }

    lastScrollTop = st;
}

    /* Flex Destroy */
    function flexdestroy(selector) {
        var el = $(selector);
        var elClean = el.clone();

        elClean.find('.flex-viewport').children().unwrap();
        elClean
            .removeClass('flexslider')
            .find('.clone, .flex-direction-nav, .flex-control-nav')
            .remove()
            .end()
            .find('*').removeAttr('style').removeClass(function (index, css) {
                return (css.match(/\bflex\S+/g) || []).join(' ');
            });

        elClean.insertBefore(el);
        el.remove();
    }

    /* Wrap Iframes in Responsive Emebed */
    function iframeEmbed(selector){
      var elem = $(selector);
      elem.wrap( "<div class='video-container'></div>" );
    }

    /* Velocity Page Load Animation */

    var isInView = function ($element) {
        var win = $(window);
        var obj = $element;
        var scrollPosition = win.scrollTop();
        var visibleArea = win.scrollTop() + win.height() + obj.data('offset') + 200;
        var objEndPos = (obj.offset().top + obj.outerHeight());

        return (visibleArea >= objEndPos && scrollPosition <= objEndPos ? true : false)
    };

  function animateContent(){
      $('section, footer .footer-contain').not('.hero').each(function(){
        var offset = $(this).outerHeight() * 0.875;
        $(this).addClass('fade').attr('data-offset',offset);
      });

      $('.item').addClass('content-animate');

      $(window).ready(function(){
        $('section:first').addClass('fade-in').find('.content-animate').velocity('transition.slideRightIn',{
           stagger:150,
           delay:500,
           visibility:'visible'
        });
      });

      $(window).on('scroll load', function () {
        $('section,footer .footer-contain').each(function (key, val) {
            var $el = $(this);

            if(!$el.hasClass("fade-in") && isInView($el)) {
              $el.addClass('fade-in').find('.content-animate').velocity('transition.slideRightIn',{
                 stagger:150,
                 delay:500,
                 visibility:'visible'
              });
            }

            if(!$el.hasClass("fade-in") && isInView($el)) {
              $el.addClass('fade-in').find('.content-animate').velocity('transition.slideRightIn',{
                 stagger:150,
                 delay:500,
                 visibility:'visible'
              });
            }

        });
    });

  }

/*
  Set Session Cookie
  */

  function sessionCookie(key){
    if ($.cookie(key) != 'true'){
      /*$('#popup').modal('show');*/
      $.cookie(key, 'true', { expires: 15 });
    }
  }


  

  function popupVideo(wrapper,element){
    var elem = $(element),
        wrapper = $(wrapper);

    $('.playVideo').click(function(){
      wrapper.velocity("fadeOut", { duration: 1000 })
      elem.velocity("fadeIn", {  delay: 999, duration: 1000 })
    })
  }

  // Mega Menu Area Hover
function subMenu(selector){
    $(selector).hover(
      function(){
      var elem = $(this).find('a'),
          tar = elem.data('hover');
      $('.main-menu li, .sub-menu').removeClass('active');
      $('#' + tar).addClass('active');
      $(this).addClass('active');
    }
    )
    $('nav').mouseleave(function(){
      $('.main-menu li, .sub-menu').removeClass('active');
    })
  }


// Cool Input Labels

function inputLabel(inputType){
    $(inputType).each(function(){
      var $this = $(this);
      // on focus add cladd active to label
      $this.focus(function(){
        $this.next().addClass("active");
      });
      //on blur check field and remove class if needed
      $this.blur(function(){
        if($this.val() === '' || $this.val() === 'blank'){
          $this.next().removeClass();
        }
      });
    });
  }
  
function mixIt(id){

  // Instantiate MixItUp:

  $(id).mixItUp();

}


  // To keep our code clean and modular, all custom functionality will be contained inside a single object literal called "buttonFilter".

var buttonFilter = {
  
  // Declare any variables we will need as properties of the object
  
  $filters: null,
  $reset: null,
  groups: [],
  outputArray: [],
  outputString: '',
  
  // The "init" method will run on document ready and cache any jQuery objects we will need.
  
  init: function(){
    var self = this; // As a best practice, in each method we will asign "this" to the variable "self" so that it remains scope-agnostic. We will use it to refer to the parent "buttonFilter" object so that we can share methods and properties between all parts of the object.
    
    self.$filters = $('#Filters');
    self.$reset = $('#Reset');
    self.$container = $('#container');
    
    self.$filters.find('.fieldset').each(function(){
      self.groups.push({
        $buttons: $(this).find('.filter'),
        active: ''
      });
    });
    
    self.bindHandlers();
  },
  
  // The "bindHandlers" method will listen for whenever a button is clicked. 
  
  bindHandlers: function(){
    var self = this;
    
    // Handle filter clicks
    
    self.$filters.on('click', '.filter', function(e){
      e.preventDefault();
      
      var $button = $(this);
      
      // If the button is active, remove the active class, else make active and deactivate others.
      
      $button.hasClass('active') ?
        $button.removeClass('active') :
        $button.addClass('active').siblings('.filter').removeClass('active');
      
      self.parseFilters();
    });
    
    // Handle reset click
    
    self.$reset.on('click', function(e){
      e.preventDefault();
      
      self.$filters.find('.filter').removeClass('active');
      
      self.parseFilters();
    });
  },
  
  // The parseFilters method checks which filters are active in each group:
  
  parseFilters: function(){
    var self = this;
 
    // loop through each filter group and grap the active filter from each one.
    
    for(var i = 0, group; group = self.groups[i]; i++){
      group.active = group.$buttons.filter('.active').attr('data-filter') || '';
    }
    
    self.concatenate();
  },
  
  // The "concatenate" method will crawl through each group, concatenating filters as desired:
  
  concatenate: function(){
    var self = this;
    
    self.outputString = ''; // Reset output string
    
    for(var i = 0, group; group = self.groups[i]; i++){
      self.outputString += group.active;
    }
    
    // If the output string is empty, show all rather than none:
    
    !self.outputString.length && (self.outputString = 'all'); 
    
    console.log(self.outputString); 
    
    // ^ we can check the console here to take a look at the filter string that is produced
    
    // Send the output string to MixItUp via the 'filter' method:
    
    if(self.$container.mixItUp('isLoaded')){
      self.$container.mixItUp('filter', self.outputString);
    }
  }
};
  
// On document ready, initialise our code.

$(function(){
      
  // Initialize buttonFilter code
      
  buttonFilter.init();
      
  // Instantiate MixItUp
      
    
});
  



  /* Init */

function pageLoad(){
    $(".fancybox").fancybox();

    $(".gallery li").lazyload({
      effect : "fadeIn"
    });


    $("#mobile-menu").menumaker({
        format: "multitoggle"
      });
    $('li.has-sub > a').attr('href',"");

    iframeEmbed('iframe');
    sessionCookie('visited');

}


$(document).ready(function(){
  pageLoad();
    $('#container').mixItUp({
    controls: {
      enable: false // we won't be needing these
    },
    callbacks: {
      onMixFail: function(){
        alert('No items were found matching the selected filters.');
      }
    }
  });

})